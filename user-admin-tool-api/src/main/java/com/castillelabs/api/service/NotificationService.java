/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castillelabs.api.service;

import com.castillelabs.entities.Notification;
import org.springframework.stereotype.Service;

/**
 *
 * @author root
 */
public interface NotificationService {
    
    public void addNotification(Notification notification);
}
