package com.castillelabs.api.service;


import com.castillelabs.entities.user.User;
import java.util.List;
import javax.ejb.Local;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * @author noorefatemah.suhabuth@castillelabs.com on 26/05/2016.
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)

public interface UserService {

    public User addUser(User user) ;
    public User updateUser(User user) ;
    public boolean delete(long id);
    public User searchUserByUsername(String username) ;
    public List<User> searchUserListByUsername(String username);
    public List<User> retrieveAllUser()  ;
    public User searchUserById(Long id)  ;
    public boolean isUserExist(String username);
    public List<User> searchByRole(String roleName);

    public List<User> searchByLastName(String lastName);

    public List<User> searchByFirstName(String firstName);
    
    


}
