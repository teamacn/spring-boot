package com.castillelabs.service.service;

import com.castillelabs.api.service.UserService;

import com.castillelabs.entities.dao.UserRepository;
import com.castillelabs.entities.user.Role;
import com.castillelabs.entities.user.User;


import ch.qos.logback.classic.Logger;

import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * @author noorefatemah.suhabuth@castillelabs.com on 01/06/2016.
 */

@Service(value = "userService")
public class UserServiceImpl   implements UserService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ShaPasswordEncoder shaEncoder;
	 private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(UserServiceImpl.class);
	
         
         
         @Override
	public User addUser(User user) {
           
		String password = user.getPassword();
		user.setPassword(shaEncoder.encodePassword(password, user.getUsername()));
		if (userRepository.isUserExist(user.getUsername()).isEmpty()) {

			for (Role role : user.getRoles()) {
				role.setUser(user);
			}
			User newUser = userRepository.save(user);
                        
			newUser.setSaved(true);
                       
			return newUser;

		} else {
			user.setSaved(false);

			return user;
		}
	}

	@Override
	public User updateUser(User user) {
		String password = user.getPassword();
		user.setPassword(shaEncoder.encodePassword(password, user.getUsername()));
		try{
			User updatedUser = userRepository.save(user);
			updatedUser.setSaved(true);
			return updatedUser;
		}
		catch(Exception e){
			user.setSaved(false);
			return user;
		}

	}

	@Override
	public boolean delete(long id) {
		User user = userRepository.findOne(id);
		try{
			userRepository.delete(id);
			LOGGER.info("User deleted !");
			return true; 
		}
		catch(Exception e){
			LOGGER.error(e.getMessage());
			return false;
		}
		
	}

	@Override
	public User searchUserByUsername(String username) {
		return userRepository.searchUserByUsername(username);
	}

	@Override
	public List<User> retrieveAllUser() {

		return userRepository.retrieveAll();

	}

	@Override
	public User searchUserById(Long id) {
		return userRepository.findOne(id);
	}

	@Override
	public boolean isUserExist(String username) {
		List<User> user = userRepository.isUserExist(username);
		if (user.isEmpty()){
			return false; 
		}
		else{
			return true;
		}
	}

	@Override
	public List<User> searchByRole(String roleName) {
		return userRepository.searchByRole(roleName);

	}

	@Override
	public List<User> searchByLastName(String lastName) {
		return userRepository.searchByLastName(lastName);

	}

	@Override
	public List<User> searchByFirstName(String firstName) {
		return userRepository.searchByFirstName(firstName);
		
	}

	@Override
	public List<User> searchUserListByUsername(String username) {
		return userRepository.searchByUsername(username);
	}

}
