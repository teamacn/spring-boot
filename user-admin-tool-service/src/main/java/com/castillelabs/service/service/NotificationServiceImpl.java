/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castillelabs.service.service;

import com.castillelabs.api.service.NotificationService;
import com.castillelabs.entities.Notification;
import com.castillelabs.entities.dao.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author root
 */
@Service("notificationService")
public class NotificationServiceImpl implements NotificationService{
@Autowired
private NotificationRepository notificationRepository ;  
    @Override
    public void addNotification(Notification notification) {
       notificationRepository.save(notification);
    }
    
}
