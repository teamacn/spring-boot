/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user.service.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import user.service.model.Username;

/**
 *
 * @author Robert
 */
public interface UsernameService extends PagingAndSortingRepository<Username, String> {

}
