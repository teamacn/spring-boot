/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user.service.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Robert
 */
@XmlRootElement
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class UsernameRole implements Serializable {

    @Id
    @GeneratedValue
    Long id;
    String username;
    String userrole;

    transient Boolean saved;
}
