/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user.service.endpoint;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Component;
import user.service.model.Username;
import user.service.model.UsernameRole;
import user.service.repository.UsernameRoleService;
import user.service.repository.UsernameService;

/**
 *
 * @author Robert
 */
@Slf4j
@Path("/userservice")
@Component
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class UserServiceEndpoint {

    @Autowired
    private UsernameService usernameService;
    @Autowired
    private UsernameRoleService usernameRoleService;

    @Autowired
    private ShaPasswordEncoder shaPasswordEncoder;

    @Path("/username")
    @GET
    public Iterable<Username> loadUsernames() {
        return usernameService.findAll();
    }

    @Path("/username/{username}")
    @GET
    public Username loadUsername(@PathParam("username") String username) {
        return usernameService.findOne(username);
    }

    @Path("/username")
    @POST
    public Username saveUsername(Username username) {
        String hash = shaPasswordEncoder.encodePassword(username.getPassword(), null);
        username.setPassword(hash);
        Username usernameSaved = usernameService.save(username);
        usernameSaved.setSaved(Boolean.TRUE);
        return usernameSaved;
    }

    @Path("/userrole")
    @GET
    public List<String> findRoles() {
        return usernameRoleService.findRoles();
    }

    @Path("/userrole")
    @POST
    public UsernameRole saveUsernameRole(UsernameRole usernameRole) {
        UsernameRole usernameRoleSaved = usernameRoleService.save(usernameRole);
        usernameRoleSaved.setSaved(Boolean.TRUE);
        return usernameRoleSaved;
    }
}
