/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user.service.config;

import com.mysql.jdbc.Driver;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 *
 * @author Robert
 */
@Configuration
@EnableJpaRepositories(basePackages = {"user.service.repository"})
public class DataBaseConfig {

    @Value("${user.service.db.url}")
    private String dbURL;
    @Value("${user.service.db.user}")
    private String dbUser;
    @Value("${user.service.db.password}")
    private String dbPass;

    @Bean
    public DataSource userServiceDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource(dbURL, dbUser, dbPass);
        dataSource.setDriverClassName(Driver.class.getCanonicalName());
        return dataSource;
    }
    
}
