/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user.service.endpoint;

import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.Matchers.*;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import user.service.model.Username;
import user.service.model.UsernameRole;
import user.service.repository.UsernameRoleService;
import user.service.repository.UsernameService;

/**
 *
 * @author Robert
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceEndpointTest {

    @InjectMocks
    UserServiceEndpoint userServiceEndpoint;

    @Mock
    UsernameService usernameService;
    @Mock
    UsernameRoleService usernameRoleService;
    @Mock
    ShaPasswordEncoder shaPasswordEncoder;

    private List<Username> usernames;
    private List<String> roles;

    @Before
    public void setUp() {

        usernames = new ArrayList<>();
        usernames.add(new Username("Rob", "Rob", Boolean.TRUE, Boolean.FALSE));
        roles = new ArrayList<>();
        roles.add("ADMIN");

        setUpMocks();

    }

    private void setUpMocks() {
        when(usernameService.findAll()).thenReturn(usernames);
        when(usernameService.findOne(eq("Rob"))).thenReturn(usernames.stream().findFirst().get());
        when(shaPasswordEncoder.encodePassword(eq("Robert"), any())).thenReturn("hashedPW");
        when(usernameRoleService.findRoles()).thenReturn(roles);
    }

    @After
    public void tearDown() {
        usernames = null;
    }

    @Test
    public void testLoadUsernames() {
        Iterable<Username> usernames = userServiceEndpoint.loadUsernames();
        assertThat(usernames, hasItem(this.usernames.stream().findFirst().get()));
    }

    @Test
    public void testLoadUsername() {
        Username username = userServiceEndpoint.loadUsername("Rob");
        assertThat(username, is(usernames.stream().findFirst().get()));
    }

    @Test
    public void testSaveUsername() {
        Username input = new Username("Robert", "Robert", Boolean.TRUE, Boolean.FALSE);

        when(usernameService.save(eq(input))).thenReturn(input);

        Username username = userServiceEndpoint.saveUsername(input);
        assertThat(username.getUsername(), is("Robert"));
        assertThat(username.getPassword(), is("hashedPW"));
        assertThat(username.getSaved(), is(true));
    }

    @Test
    public void testFindRoles() {
        List<String> roles = userServiceEndpoint.findRoles();
        assertThat(roles, hasItem("ADMIN"));
    }

    @Test
    public void testSaveUsernameRole() {
        UsernameRole input = new UsernameRole(0l, "Rob", "ADMIN", Boolean.FALSE);

        when(usernameRoleService.save(eq(input))).thenReturn(input);

        UsernameRole output = userServiceEndpoint.saveUsernameRole(input);

        assertThat(output.getUsername(), is("Rob"));
        assertThat(output.getUserrole(), is("ADMIN"));
        assertThat(output.getSaved(), is(true));
    }

}
