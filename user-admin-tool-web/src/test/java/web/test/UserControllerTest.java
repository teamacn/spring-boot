//package web.test;
//
//import com.castillelabs.api.service.UserService;
//import com.castillelabs.entities.user.Role;
//import com.castillelabs.entities.user.User;
//import com.castillelabs.presentation.web.controller.UserRestController;
//import java.util.ArrayList;
//import java.util.List;
//import org.hamcrest.Matchers;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import static org.mockito.Matchers.eq;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.runners.MockitoJUnitRunner;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
//import org.springframework.validation.BindingResult;
//
///**
// * @author noorefatemah.suhabuth@castillelabs.com on 11/07/2016.
// */
//@RunWith(MockitoJUnitRunner.class)
//public class UserControllerTest {
//     @InjectMocks
//     UserRestController userRestController;
//    @Mock
//    UserService userService;
//    @Mock
//    ShaPasswordEncoder shaPasswordEncoder;
//
//    private User user;
//    private List<User> userList = new ArrayList<User>();
//    private List<Role> roleList = new ArrayList<Role>();
//
//    @Before
//    public void setUp() {
//
//        Role role = new Role();
//        role.setRoleName("ROLE_REST");
//        roleList.add(role);
//        user = new User("noore", "suhabuth", "noor", "password", "noore@gmail,com", roleList, false);
//        userList.add(user);
//        setUpMocks();
//
//    }
//
//    @After
//    public void tearDown() {
//        user = null;
//        userList = null;
//        roleList = null;
//
//    }
//
//    public void setUpMocks() {
//        Mockito.when(userService.retrieveAllUser()).thenReturn(userList);
//        Mockito.when(userService.searchUserByUsername(eq("noor"))).thenReturn(userList.stream().findFirst().get());
//        Mockito.when(userService.searchUserById(eq(1l))).thenReturn(userList.stream().findFirst().get());
//           Mockito.when(userService.isUserExist(eq("noore_07"))).thenReturn(true);
//
//    }
//
//    @Test
//    public void getUsers() {
//        ResponseEntity<List<User>> userLists =  userRestController.listAllUsers();
//
//        Assert.assertThat(userLists.getBody().stream().findFirst().get().getFirstName(), Matchers.is(userList.stream().findFirst().get().getFirstName()));
//        Assert.assertThat(userLists.getBody().stream().findFirst().get().getLastName(), Matchers.is(userList.stream().findFirst().get().getLastName()));
//        Assert.assertThat(userLists.getBody().stream().findFirst().get().getUsername(), Matchers.is(userList.stream().findFirst().get().getUsername()));
//        Assert.assertThat(userLists.getBody().stream().findFirst().get().getEmailAddress(), Matchers.is(userList.stream().findFirst().get().getEmailAddress()));
//        Assert.assertThat(userLists.getBody().stream().findFirst().get().getRoles().get(0).getRoleName(), Matchers.is(userList.stream().findFirst().get().getRoles().get(0).getRoleName()));
//        Assert.assertThat(userLists.getBody().stream().findFirst().get().getPassword(), Matchers.is(userList.stream().findFirst().get().getPassword()));
//        Assert.assertEquals(200, Integer.parseInt(userLists.getStatusCode().toString()));
//    }
//
//    @Test
//    public void noUsers(){
//        Mockito.when(userService.retrieveAllUser()).thenReturn(new ArrayList<User>());
//    ResponseEntity<List<User>> userLists =  userRestController.listAllUsers();
//
//      Assert.assertEquals(204, Integer.parseInt(userLists.getStatusCode().toString()));
//    }
//
//    @Test
//    public void getUsersByUsername() {
//         ResponseEntity<User> retreivedUser = userRestController.getUserByUsername(user.getUsername());
//
//
//        Assert.assertThat(retreivedUser.getBody().getFirstName(), Matchers.is(this.userList.stream().findFirst().get().getFirstName()));
//        Assert.assertThat(retreivedUser.getBody().getLastName(), Matchers.is(this.userList.stream().findFirst().get().getLastName()));
//        Assert.assertThat(retreivedUser.getBody().getUsername(), Matchers.is(this.userList.stream().findFirst().get().getUsername()));
//        Assert.assertThat(retreivedUser.getBody().getEmailAddress(), Matchers.is(this.userList.stream().findFirst().get().getEmailAddress()));
//        Assert.assertThat(retreivedUser.getBody().getRoles().get(0).getRoleName(), Matchers.is(this.userList.stream().findFirst().get().getRoles().get(0).getRoleName()));
//        Assert.assertThat(retreivedUser.getBody().getPassword(), Matchers.is(this.userList.stream().findFirst().get().getPassword()));
//        Assert.assertEquals(200, Integer.parseInt(retreivedUser.getStatusCode().toString()));
//    }
//
//    @Test
//    public void getUserById() {
//        ResponseEntity<User> retrivedUser = userRestController.getUser(1l);
//        Assert.assertThat(retrivedUser.getBody().getFirstName(), Matchers.is(this.userList.stream().findFirst().get().getFirstName()));
//        Assert.assertThat(retrivedUser.getBody().getLastName(), Matchers.is(this.userList.stream().findFirst().get().getLastName()));
//        Assert.assertThat(retrivedUser.getBody().getUsername(), Matchers.is(this.userList.stream().findFirst().get().getUsername()));
//        Assert.assertThat(retrivedUser.getBody().getEmailAddress(), Matchers.is(this.userList.stream().findFirst().get().getEmailAddress()));
//        Assert.assertThat(retrivedUser.getBody().getRoles().get(0).getRoleName(), Matchers.is(this.userList.stream().findFirst().get().getRoles().get(0).getRoleName()));
//        Assert.assertThat(retrivedUser.getBody().getPassword(), Matchers.is(this.userList.stream().findFirst().get().getPassword()));
//        Assert.assertEquals(200, Integer.parseInt(retrivedUser.getStatusCode().toString()));
//    }
//
//    @Test
//    public void addUser() {
//        Role role = new Role();
//        role.setRoleName("ROLE_REST");
//        roleList.add(role);
//        User newUser = new User("noore", "suhabuth", "noorefatemah_S", "password", "noore@gmail,com", roleList, false);
//        Mockito.when(userService.addUser(eq(newUser))).thenReturn(newUser);
//        BindingResult result = null;
//        ResponseEntity<User> saveUser = userRestController.createUser(newUser ,result);
//        boolean added = true;
//        Assert.assertThat(added, Matchers.is(saveUser.getBody().isSaved()));
//        Assert.assertThat(newUser, Matchers.is(saveUser.getBody()));
//        Assert.assertEquals(201, Integer.parseInt(saveUser.getStatusCode().toString()));
//
//    }
//
//    @Test
//    public void userExists() {
//        Role role = new Role();
//        role.setRoleName("ROLE_REST");
//        roleList.add(role);
//        user = new User("noore", "suhabuth", "noore_07", shaPasswordEncoder.encodePassword("password", "hello"), "noore@gmail,com", roleList, false);
//        Mockito.when(userService.addUser(eq(user))).thenReturn(user);
//        BindingResult result = null;
//        ResponseEntity<User> saveUser = userRestController.createUser(user,new Bi);
//
//        Assert.assertEquals(409, Integer.parseInt(saveUser.getStatusCode().toString()));
//
//
//    }
//
//    @Test
//    public void updateUserTest() {
//        Role role = new Role();
//        role.setRoleName("ROLE_EDITOR");
//        roleList.add(role);
//        User newUser = new User("noorefatemah", "Suhabuth", "noore1", "oasssss", "noore0792@live.com", roleList, false);
//        Mockito.when(userService.updateUser(eq(newUser))).thenReturn(newUser);
//        newUser.setId(1l);
//        BindingResult bindingResult = null;
//        ResponseEntity<User> saveUser = userRestController.updateUser(1l,newUser);
//        boolean added = true;
//        Assert.assertThat(newUser, Matchers.is(saveUser.getBody()));
//        Assert.assertEquals(200, Integer.parseInt(saveUser.getStatusCode().toString()));
//
//    }
//
//    @Test
//    public void deleteUserTest() {
//        user = userRestController.getUser(1l).getBody();
//        user.setId(1l);
//       ResponseEntity<User> deletedUser =  userRestController.deleteUser(user.getId());
//        Mockito.when(userService.delete(user.getId())).thenReturn(userList.remove(user));
//        Assert.assertEquals(0, userList.size());
//        Assert.assertEquals(204, Integer.parseInt(deletedUser.getStatusCode().toString()));
//    }
//
//    @Test
//    public void deleteUnexistedUser() {
//        Mockito.when(userService.searchUserById(2l)).thenReturn(new User());
//        Mockito.when(userService.delete(2l)).thenReturn(Boolean.FALSE);
//        ResponseEntity<User> deletedUser = userRestController.deleteUser(2l);
//        Assert.assertEquals(404, Integer.parseInt(deletedUser.getStatusCode().toString()));
//    }
//
//    @Test
//    public void updatedUserNotExist() {
//        User newUser = new User("noorefatemah", "Suhabuth", "noore1", "oasssss", "noore0792@live.com", roleList, false);
//        Mockito.when(userService.updateUser(eq(newUser))).thenReturn(new User());
//        Mockito.when(userService.searchUserById(3l)).thenReturn(null);
//        BindingResult bindingResult = null;
//        ResponseEntity<User> saveUser = userRestController.updateUser(3l, new User());
//        Assert.assertEquals(404, Integer.parseInt(saveUser.getStatusCode().toString()));
//    }
//
//    @Test
//    public void userDoesNotExists() {
//
//        Mockito.when(userService.searchUserByUsername(user.getUsername())).thenReturn(null);
//        ResponseEntity<User> userRet = userRestController.getUserByUsername(user);
//        Assert.assertEquals(404, Integer.parseInt(userRet.getStatusCode().toString()));
//    }
//    @Test
//    public void userDoesNotExistsId() {
//
//        Mockito.when(userService.searchUserById(2l)).thenReturn(null);
//        ResponseEntity<User> userRet = userRestController.getUser(2l);
//        Assert.assertEquals(404, Integer.parseInt(userRet.getStatusCode().toString()));
//    }
//
//}
