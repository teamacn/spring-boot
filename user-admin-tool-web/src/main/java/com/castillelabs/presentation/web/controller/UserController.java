package com.castillelabs.presentation.web.controller;

import java.util.Map;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * @author noorefatemah.suhabuth@castillelabs.com on 30/05/2016.
 */

@Controller
public class UserController {
   
    @RequestMapping(value = "/home",method = RequestMethod.GET)
    public String returnHomePage() {

       return "jsp/user";
    }

    @RequestMapping(value = "/signin", method = RequestMethod.GET)
    public String login() {
       return "jsp/logiN";

    }

    @RequestMapping(value = "/loginFailure", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap model) {
        model.addAttribute("errorMessage", "Wrong username password combination! Please login with correct credentials and ensure that you have the required access!");
        return "jsp/logiN";
    }    



}


