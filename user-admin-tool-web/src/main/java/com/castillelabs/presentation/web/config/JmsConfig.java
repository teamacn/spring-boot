package com.castillelabs.presentation.web.config;

import javax.jms.Destination;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MarshallingMessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

/**
 * @author noorefatemah.suhabuth@castillelabs.com on 09/09/2016.
 */
@Configuration
@EnableJms
public class JmsConfig {

    private static final String DEFAULT_BROKER_URL = "tcp://localhost:61616";
    private static final String QUEUE_NAME = "mailQueue";
    private static final String TOPIC_NAME = "mailTopic";

    @Autowired
    private JmsTemplate jmsTemplate;

    @Bean
    public ActiveMQConnectionFactory connectionFactory() {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
        connectionFactory.setBrokerURL(DEFAULT_BROKER_URL);

        return connectionFactory;
    }

    @Bean
    public JmsTemplate jmsTemplate() {
        JmsTemplate template = new JmsTemplate();
        template.setConnectionFactory(connectionFactory());
        template.setDefaultDestinationName(QUEUE_NAME);
        template.setMessageConverter(messageConverter());

        return template;
    }

        @Bean
    public JmsTemplate jmsTopicTemplate() {
        JmsTemplate jmsTopicTemplate = new JmsTemplate();
        jmsTopicTemplate.setConnectionFactory(connectionFactory());
        jmsTopicTemplate.setDefaultDestination(activeMwMQTopic());
        jmsTopicTemplate.setMessageConverter(messageConverter());
        jmsTopicTemplate.setPubSubDomain(true);
        return jmsTopicTemplate;
    }

    @Bean
    public MessageConverter messageConverter() {
        MarshallingMessageConverter converter = new MarshallingMessageConverter();
        converter.setMarshaller(marshaller());
        converter.setUnmarshaller(marshaller());

        return converter;
    }

    @Bean

    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setPackagesToScan("com.castillelabs");

        return marshaller;
    }



    @Bean
    public ActiveMQTopic activeMwMQTopic() {
        ActiveMQTopic activeMQTopic = new ActiveMQTopic();
        activeMQTopic.setPhysicalName(TOPIC_NAME);
        return activeMQTopic;
    }

    @Bean
    public JmsListenerContainerFactory jmsListenerContainerFactory() {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        factory.setPubSubDomain(Boolean.TRUE);
        factory.setMessageConverter(messageConverter());
        return factory;
    }

    @Bean
    public JmsListenerContainerFactory queueJmsListenerContainerFactory() {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        factory.setMessageConverter(messageConverter());
        return factory;
    }

}
