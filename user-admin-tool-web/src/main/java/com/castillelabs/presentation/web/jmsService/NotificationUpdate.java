/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castillelabs.presentation.web.jmsService;

import com.castillelabs.api.service.NotificationService;
import com.castillelabs.api.service.UserService;
import com.castillelabs.entities.Notification;
import com.castillelabs.entities.enums.NotificationType;
import com.castillelabs.entities.user.User;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 *
 * @author root
 */
@Component("notificationUpdate")
public class NotificationUpdate {

    @Autowired
    private NotificationService notificationService;
    @Autowired
    private UserService userService;

    @JmsListener(destination = "mailTopic", containerFactory = "jmsListenerContainerFactory")
    public void receive(User user) {
        List<User> users = userService.retrieveAllUser();
        int length = users.size() - 1;
        for (int i = 0; i <= length - 1; i++) {

            Notification notification = new Notification();
            notification.setUserId(users.get(i).getId());
            notification.setNotificationType(NotificationType.NEW_USER_NOTIFICATION);
            notificationService.addNotification(notification);
        }

    }

}
