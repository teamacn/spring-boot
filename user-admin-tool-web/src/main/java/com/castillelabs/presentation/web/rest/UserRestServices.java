package com.castillelabs.presentation.web.rest;

import com.castillelabs.api.service.UserService;
import com.castillelabs.entities.user.User;
import java.util.List;
import javax.ws.rs.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * @author noorefatemah.suhabuth@castillelabs.com on 29/06/2016.
 */
@Path("/")
public class UserRestServices  extends SpringBeanAutowiringSupport {

    @Autowired
    private UserService userService;

    @GET
    @Path("/getUsers")
    @Produces("application/json")
    public List<User> getAllUsers() {
        List<User> userModelList;
        userModelList = userService.retrieveAllUser();
       
        return userModelList;
    }

    @POST
    @Path("/createUser")
    @Consumes("application/json")
    @Transactional
    public User createUser(User user) {
        User newUser = userService.addUser(user);
        newUser.setSaved(Boolean.TRUE);
        return newUser;
        
    }

    @PUT
    @Path("/updateUser")
    @Consumes("application/json")
    @Transactional
    public User updateUser(User user) {
          User newUser = userService.updateUser(user);
          newUser.setSaved(Boolean.TRUE);
          return newUser;
    
    }

    @GET
    @Produces("application/json")
    @Path("/searchUserById/{userid}")
    public User searchById(@PathParam("userid") long userid){
      return userService.searchUserById(userid);

    }

    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("/searchByUsername/{username}")
    public User searchByUsername(@PathParam("username") String username){
        return userService.searchUserByUsername(username);
        
    }

    @DELETE
    @Path("/delete/{userid}")
        public boolean deleteUser(@PathParam("userid") long userid){
        return userService.delete(userid);
    }



}