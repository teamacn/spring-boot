package com.castillelabs.presentation.web.controller;

import ch.qos.logback.classic.Logger;
import com.castillelabs.api.request.EmailRequest;
import com.castillelabs.api.request.EmailTemplate;

import com.castillelabs.api.service.UserService;

import java.util.List;
import javax.validation.Valid;

import com.castillelabs.entities.user.User;
import com.castillelabs.presentation.web.jmsService.JmsService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@RestController

public class UserRestController {

    @Autowired
    private UserService userService;  //Service which will do all data retrieval/manipulation work
    private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(UserRestController.class);
    @Autowired
    private JmsService jmsService;

    @RequestMapping(value = "/user/", method = RequestMethod.GET)
    public ResponseEntity<List<User>> listAllUsers() {

        List<User> users = userService.retrieveAllUser();
        if (users.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    //-------------------Retrieve Single User--------------------------------------------------------
    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUser(@PathVariable("id") long id) {
        LOGGER.info("Fetching user with id : " + id);
        User user = userService.searchUserById(id);
        LOGGER.info("User with id " + id + " found");
        if (user.getId() == null) {

            return new ResponseEntity<>(user, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/getUserByUsername/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUserByUsername(@PathVariable("username") String username) {
        User retrivedUser = userService.searchUserByUsername(username);
        if (retrivedUser != null) {
            return new ResponseEntity<>(retrivedUser, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @RequestMapping(value = "/getUserListByFirstName/{firstName}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<User>> listUsersByFirstname(@PathVariable("firstName") String firstName) {
        List<User> users = userService.searchByFirstName(firstName);
        if (users.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @RequestMapping(value = "/getUserListByLastName/{lastName}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<User>> listUsersByLastName(@PathVariable("lastName") String lastName) {

        List<User> users = userService.searchByLastName(lastName);
        if (users.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @RequestMapping(value = "/getUserListByUserName/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<User>> listUserByUsername(@PathVariable("username") String username) {

        List<User> users = userService.searchUserListByUsername(username);
        if (users.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<>(users, HttpStatus.OK);

    }

    @RequestMapping(value = "/getUserListByRole/{role}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<User>> listUserByRole(@PathVariable("role") String roleName) {

        List<User> users = userService.searchByRole(roleName);
        if (users.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<>(users, HttpStatus.OK);

    }

    //-------------------Create a User--------------------------------------------------------
    @RequestMapping(value = "/createUser", method = RequestMethod.POST)
    public ResponseEntity<User> createUser(@Valid @RequestBody User user, BindingResult bindingResult) {
        LOGGER.info("Field Error :" + String.valueOf(bindingResult));
        LOGGER.info(bindingResult.toString());
        EmailRequest request = new EmailRequest();
        if (!bindingResult.hasErrors()) {

            if (userService.isUserExist(user.getUsername())) {

                return new ResponseEntity<User>(HttpStatus.CONFLICT);
            } else {
                User savedUser = userService.addUser(user);
                savedUser.setSaved(true);
                request.setEmailTemplate(EmailTemplate.REGISTRATION_EMAIL);
                request.setUsername(user.getUsername());
                jmsService.sendMessage(request);
                LOGGER.info(String.valueOf(savedUser.getId()));
                jmsService.sendMessageToTopic(savedUser);
                return new ResponseEntity<User>(savedUser, HttpStatus.CREATED);
            }
        } else {
            return new ResponseEntity<User>(user, HttpStatus.BAD_REQUEST);

        }

    }

    //------------------- Update a User --------------------------------------------------------
    @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
    public ResponseEntity<User> updateUser(@PathVariable("id") long id, @Valid @RequestBody User user) {
        LOGGER.info("Updating User " + id);
        EmailRequest request = new EmailRequest();
        User currentUser = userService.searchUserById(id);
        if (currentUser != null) {
            userService.updateUser(user);
            request.setEmailTemplate(EmailTemplate.UPDATED_DETAILS);
            request.setUsername(user.getUsername());
            jmsService.sendMessage(request);
            return new ResponseEntity<>(user, HttpStatus.OK);

        } else {
            LOGGER.info("User with id " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteUser(@PathVariable("id") long id) {
        LOGGER.info("Fetching & Deleting User with id " + id);
        User user = userService.searchUserById(id);
        if (user.getId() != null) {
            LOGGER.info("Unable to delete. User with id " + id + " not found");
            userService.delete(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
}
