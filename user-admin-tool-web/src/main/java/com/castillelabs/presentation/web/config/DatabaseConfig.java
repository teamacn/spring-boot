/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castillelabs.presentation.web.config;

import com.mysql.jdbc.Driver;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 *
 * @author root
 */
@Configuration
@EnableJpaRepositories(basePackages = "com.castillelabs.entities.dao", considerNestedRepositories = true)


public class DatabaseConfig {

    @Value("${user.service.db.url}")
    private String dbUrl;
    @Value("${user.service.username}")
    private String username;
    @Value("${user.service.password}")
    private String password;

    @Bean
    public DataSource dataSource() {

        DriverManagerDataSource dataSource = new DriverManagerDataSource(dbUrl, username, password);
        dataSource.setDriverClassName(Driver.class.getCanonicalName());
        return dataSource;
    }


    @Bean
    public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
        PropertySourcesPlaceholderConfigurer c = new PropertySourcesPlaceholderConfigurer();
        c.setLocation(new ClassPathResource("application.properties"));
        return c;
    }


 



}
