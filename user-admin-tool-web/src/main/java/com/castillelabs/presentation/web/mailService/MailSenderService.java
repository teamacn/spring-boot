/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castillelabs.presentation.web.mailService;

import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

/**
 *
 * @author Noorefatemah Suhabut
 */
@Component
public class MailSenderService {
    @Autowired
    private JavaMailSender mailSender; 
    private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(MailSenderService.class);
    
    
    @Value("${background.image}")
    private String backgroundImg;
    @Value("${logo.background.image}")
    private String logo_image;
    @Value("${logo.thymeleaf}")
    private String logo_thymeleaf;
    @Value("${thymeleaf.banner}")
    private  String thymeleaf_banner ;
    
     private static final String PNG_MIME = "image/png";
    
    
     public boolean sendHtml(String to, String subject, String htmlBody) {
        return sendMail(to, subject, htmlBody, true);
    }
  private boolean sendMail(String to, String subject, String text, Boolean isHtml) {
        try {
            MimeMessage mail = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(text, isHtml);
            helper.addInline("background", new ClassPathResource(backgroundImg), PNG_MIME);
            helper.addInline("logo-background", new ClassPathResource(logo_image), PNG_MIME);
            helper.addInline("thymeleaf-logo", new ClassPathResource(logo_thymeleaf), PNG_MIME);
            helper.addInline("thymeleaf-banner", new ClassPathResource(thymeleaf_banner), PNG_MIME);
            mailSender.send(mail);
            LOGGER.info("Send email '{}' to: {}", subject, to);
            return true;
        } catch (Exception e) {
            LOGGER.error(String.format("Problem with sending email to: {}, error message: {}", to, e.getMessage()));
            return false; 
        }
    }
    
}
