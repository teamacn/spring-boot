/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castillelabs.presentation.web.config;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 *
 * @author root
 */
@Configuration

public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource  dataSource;

    @Bean
      public ShaPasswordEncoder shaPasswordEncoder(){
        return new ShaPasswordEncoder(256);}

    @Autowired
    protected void configureManager (AuthenticationManagerBuilder auth) throws Exception {

         auth
            .inMemoryAuthentication().withUser("admin").password("admin").roles("ADMIN");
        auth.jdbcAuthentication().dataSource(dataSource)
                  .passwordEncoder(shaPasswordEncoder())
                .usersByUsernameQuery("select username ,password,TRUE from user where username=?")
                .authoritiesByUsernameQuery("SELECT username,role from user,user_role where user.id = user_role.user_id and user.username=?");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception { //NOSONAR
        http.authorizeRequests()
                .antMatchers("/resources/**").permitAll()
                .antMatchers("/api/soap/userWebService/**").authenticated()
                .antMatchers("/home/**").authenticated()
                .and()
                .httpBasic()
               .and().csrf().disable().sessionManagement().disable();
}


}
