/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castillelabs.presentation.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolver;

/**
 *
 * @author root
 */


@EnableWebMvc
@Configuration
@ComponentScan(basePackages = { "com.castillelabs.presentation.web.controller" })
public class WebConfig extends WebMvcConfigurerAdapter {
 
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
    }
 
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
 
    @Bean
    public InternalResourceViewResolver jspViewResolver() {
        InternalResourceViewResolver internalViewResolver = new InternalResourceViewResolver();
        internalViewResolver.setViewNames("jsp/*");
        internalViewResolver.setViewClass(org.springframework.web.servlet.view.JstlView.class);
        internalViewResolver.setPrefix("/WEB-INF/pages/");
        internalViewResolver.setSuffix(".jsp");
        internalViewResolver.setOrder(0);
        return internalViewResolver;
    }
 
    

@Bean
public  TemplateResolver emailTemplateResolver() {
    TemplateResolver templateResolver = new ClassLoaderTemplateResolver();
    templateResolver.setPrefix("/templates/");
    templateResolver.setSuffix("html");
    templateResolver.setTemplateMode("HTML5");
    templateResolver.setOrder(0);
    return templateResolver;
}


/**
 * THYMELEAF: Template Resolver for email templates.
 */


}
