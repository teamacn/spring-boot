//package com.castillelabs.presentation.web.controller;
//
//import com.castillelabs.presentation.web.jms.EmailService;
//
//import org.springframework.messaging.MessagingException;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//
//import java.io.IOException;
//
///**
// * @author noorefatemah.suhabuth@castillelabs.com on 26/08/2016.
// */
//public class EmailSenderController {
//
//    @RequestMapping(value = "/sendMailWithInlineImage", method = RequestMethod.POST)
//    public String sendMailWithInline(
//            @RequestParam("recipientName") final String recipientName,
//            @RequestParam("recipientEmail") final String recipientEmail,
//            @RequestParam("subject") final String subject,
//            @RequestParam("body") final String body,
//            @RequestParam("from") final String from)throws MessagingException, IOException, javax.mail.MessagingException {
//            EmailService emailService = new EmailService();
//            emailService.sendMailWithInline(recipientName,recipientEmail, subject, body, from);
//            return "redirect:sent.html";
//    }
//}
