package com.castillelabs.presentation.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;

/**
 * @author noorefatemah.suhabuth@castillelabs.com on 12/09/2016.
 */
@Configuration
public class webServletConfig {

  @Bean
  public ServletRegistrationBean dispatcherSerlvet(){
    return new ServletRegistrationBean(new CXFServlet(), "/api/soap/*"); 
  }
  
  
}
