/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castillelabs.presentation.web.jmsService;

import com.castillelabs.api.service.UserService;
import com.castillelabs.entities.user.User;
import com.castillelabs.presentation.web.mailService.EmailHtmlSender;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;

/**
 *
 * @author root
 */
@Component("topicEmailSender")
public class TopicEmailSender {

    @Autowired
    private EmailHtmlSender emailHtmlSender;
    @Autowired
    private UserService userService;

    @JmsListener(destination = "mailTopic", containerFactory = "jmsListenerContainerFactory")
    public void receive(User user) {
        List<User> users = userService.retrieveAllUser();
        Context context = new Context();
        context.setVariable("date", Calendar.getInstance());
        String title = "New User Notification";
        context.setVariable("title", title);
        context.setVariable("newUsername", user.getUsername());
        context.setVariable("firstName", user.getFirstName());
        context.setVariable("lastName", user.getLastName());
        int length = users.size() - 1;
        for (int i = 0; i <= length - 1; i++) {
            context.setVariable("username", users.get(i).getUsername());
            boolean sent = emailHtmlSender.send(users.get(i).getEmailAddress(), title, "template", context);

        }

    }
}
