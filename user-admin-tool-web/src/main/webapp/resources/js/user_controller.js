
myApp.controller('UserController', ['$scope', 'UserService', 'NgTableParams', '$http', function ($scope, UserService, NgTableParams, $http) {
        $scope.userExists = false;
        $scope.message = false;
        $scope.usernameNull = false;
        $scope.usernameMinLen = false;
        $scope.usernameExist = false;
        $scope.firstnameNull = false;
        $scope.firstnameMinLen = false;
        $scope.lastNameNull = false;
        $scope.lastNameMinLen = false;
        $scope.passwordNull = false;
        $scope.confirmPasswordNull = false;
        $scope.passwordMatch = false;
        $scope.emailNull = false;
        $scope.emailFormat = false;

        $scope.data = {
            singleSelect: null,
            multipleSelect: []
        };


        $scope.user = {
            id: null,
            firstName: '',
            lastname: '',
            username: '',
            password: '',
            emailAddress: '',
            roles: [
                {
                    id: null,
                    roleName: ''
                }, {
                    id: null,
                    roleName: ''
                }, {
                    id: null,
                    roleName: ''
                }, {
                    id: null,
                    roleName: ''
                }

            ]
        };
        $scope.users = [];

        $scope.fetchAllUsers = function () {
            UserService.fetchAllUsers()
                    .then(
                            function (d) {
                                $scope.users = d;
                                var data = d;
                                console.log(d);
                                $scope.usersTable = new NgTableParams({page: 1, count: 3}, {dataset: data , total:100});
                            },
                            function (errResponse) {

                            }
                    );
        };

      

        $scope.create = function (user) {
            $scope.user.roles.length = $scope.data.multipleSelect.length;
            for (var i = 0; i < $scope.data.multipleSelect.length; i++) {
                $scope.user.roles[i].roleName = ($scope.data.multipleSelect[i]);
            }
            user = $scope.user;

            $http.post('http://localhost:8080/createUser', user)
                    .success(function (data, status, headers, config)
                    {
                        document.getElementById('message').innerHTML = "The user with username : " + $scope.user.username + " has been added!";
                        $scope.reset();

                    })
                    .error(function (data, status, headers, config)
                    {
                        if (status === 400) {
                            console.log(headers);
                            console.log(config);
                            document.getElementById('message').style.color = "Red";
                            document.getElementById('message').innerHTML = "User has not been added due to wrong details provided! ";
                            $scope.reset();

                        } else if (status == 409) {
                            document.getElementById('message').style.color = "Red";
                            document.getElementById('message').innerHTML = "User cannot be added as the username already exists";
                            $scope.reset();
                        }
                    });
        };


        $scope.update = function (user,id) {

            $http.put('http://localhost:8080/user/' +id, user)
                    .success(function (data, status, headers, config)
                    { 
                        document.getElementById('message').style.color = "Green";
                        document.getElementById('message').innerHTML = "The user with username : " + $scope.user.username + " has been updated!";
                        $scope.reset();
                    })
                    .error(function (data, status, headers, config)
                    {
                        if (status === 400) {
                            document.getElementById('message').style.color = "Red";
                            document.getElementById('message').innerHTML = "User has not been updated due to wrong details provided! ";

                        } else if (status == 409) {
                            document.getElementById('message').style.color = red;
                            document.getElementById('message').innerHTML = "User cannot be added as the username already exists";
                        }
                    });
        };


        $scope.updateUser = function (user, id) {
            UserService.updateUser(user, id)
                    .then(
                            $scope.fetchAllUsers,
                            function (errResponse) {

                            }
                    );
        };

        $scope.deleteUser = function (id) {
            UserService.deleteUser(id)
                    .then(
                            $scope.fetchAllUsers,
                            function (errResponse) {

                            }
                    );
        };

        $scope.isExistUser = function (username) {
            UserService.isExistUser(username)
                    .then(
                            function (data) {
                                if (data !== true) {

                                    return false;
                                } else {
                                    return true;
                                }
                            }
                    );
        };

        $scope.fetchAllUsers();
        $scope.submit = function () {
            $scope.confirmPassword = document.getElementById('confirmPassword').value;
            $scope.password = document.getElementById('password').value;
            if ($scope.user.id === null) {
                if ($scope.user.username === null) {
                    $scope.usernameNull = true;
                } else if ($scope.user.username.length < 3) {
                    $scope.usernameMinLen = true;
                } else if (checkUser($scope.users)) {

                    $scope.usernameExist = true;
                } else if ($scope.user.firstName === null) {
                    $scope.firstnameNull = true;

                } else if ($scope.user.lastname === null) {
                    $scope.lastNameNull = true;

                } else if ($scope.user.password === null) {
                    $scope.passwordNull = true;

                } else if (document.getElementById('confirmPassword').value === null) {
                    $scope.confirmPasswordNull = true;

                } else if (document.getElementById('confirmPassword').value !== $scope.user.password) {
                    $scope.passwordMatch = true;
                } else if ($scope.user.emailAddress === null) {
                    $scope.emailNull = true;

                } else {
                    $scope.create($scope.user);
                }
            } else {

                $scope.update($scope.user,$scope.user.id);
            }

        };

        $scope.edit = function (id) {
            document.getElementById("heading").innerHTML = "Updating user with id : " + id;
            document.getElementById('password').style.enabled = false;
            document.getElementById('confirmPassword').style.enabled = false;
            for (var i = 0; i < $scope.users.length; i++) {
                if ($scope.users[i].id === id) {
                    $scope.data.multipleSelect = $scope.users[i].roles.roleName;
                    $scope.user = angular.copy($scope.users[i]);
                    break;
                }
            }
        };

        $scope.search = function (searchParamater) {
            console.log($scope.searchText);
            if (searchParamater === 'Firstname') {
                $http.get('http://localhost:8080/getUserListByFirstName/'+$scope.searchText )
                        .success(function (data, status, headers, config)
                        {
                            
                                $scope.users = data;
                                var data = data;
                                $scope.usersTable = new NgTableParams({page: 1, count: 3}, {dataset: data});
                          
                        })
                        .error(function (data, status, headers, config)
                        {
                            console.log(data);
                        });
            } else if (searchParamater === 'Lastname') {
                $http.get('http://localhost:8080/getUserListByLastName/' + $scope.searchText)
                        .success(function (data, status, headers, config)
                        {
                            
                                $scope.users = data;
                                var data = data;
                                $scope.usersTable = new NgTableParams({page: 1, count: 3}, {dataset: data});
                           

                            
                        })
                        .error(function (data, status, headers, config)
                        {
                            console.log(data);
                        });
            } else if (searchParamater === 'Username') {
                $http.get('http://localhost:8080/getUserListByUsername/' + $scope.searchText)
                        .success(function (data, status, headers, config)
                        {
                           
                                $scope.users = data;
                                var data = data;
                                $scope.usersTable = new NgTableParams({page: 1, count: 3}, {dataset: data});
                           
                               

                            
                        })
                        .error(function (data, status, headers, config)
                        {
                            console.log(data);
                        });
            } else if (searchParamater === 'Role') {
                $http.get('http://localhost:8080/getUserListByRole/' + $scope.searchText)
                        .success(function (data, status, headers, config)
                        {
                        
                                $scope.users = data;
                                var data = data;
                                $scope.usersTable = new NgTableParams({page: 1, count: 3}, {dataset: data});
                          
                        })
                        .error(function (data, status, headers, config)
                        {
                            console.log(data);
                        });
            } else {

            }

        };
  $scope.remove = function (id) {
            var response = confirm("Are you sure you want to delete the selected user ? ");
            if (response === true) {

                if ($scope.user.id === id) {
                    $scope.reset();
                }
                $scope.deleteUser(id);
                document.getElementById('message').innerHTML = "User has been deleted";
            } else {
                alert("User was not deleted! ");
            }
        };


        $scope.reset = function () {
            document.getElementById('confirmPassword').value = null;
            $scope.user = {
                id: null,
                firstName: "",
                lastname: "",
                username: "",
                password: "",
                emailAddress: "",
                roles: [
                    {
                        role: {
                            roleName: ''

                        }
                    }, {
                        roleName: ''
                    },
                    {
                        roleName: ''
                    },
                    {
                        roleName: ''
                    }
                ]

            };
            $scope.usernameNull = false;
            $scope.usernameMinLen = false;
            $scope.usernameExist = false;
            $scope.firstnameNull = false;
            $scope.firstnameMinLen = false;
            $scope.lastNameNull = false;
            $scope.lastNameMinLen = false;
            $scope.passwordNull = false;
            $scope.confirmPasswordNull = false;
            $scope.passwordMatch = false;
            $scope.emailNull = false;
            $scope.emailFormat = false;
            document.getElementById('heading').innerHTML = "Add New User";
            $scope.fetchAllUsers();
        };
        function checkUser(users) {
            for (var i = 0; i < users.length; i++) {
                if (users[i].username === document.getElementById('username').value) {
                    return true;
                } else {
                    return false;
                }
            }
        }

    }]);




