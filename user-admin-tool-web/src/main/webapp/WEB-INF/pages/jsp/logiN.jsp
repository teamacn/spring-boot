<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/include/lib.jsp"%>
<%--<%@ include file="/WEB-INF/jsp/include/libraries.jsp"%>--%>
<div class="container">
    <div class="row">
        <h1 class="text-center">User Admin Tool Login</h1>
        <h6 class="text-center"  style="color: red">${errorMessage}</h6>

        <p class="text-center"><a href="#" class="btn btn-primary btn-lg" role="button" data-toggle="modal" data-target="#login-modal">Login</a></p>
    </div>
</div>
<!-- END # BOOTSNIP INFO -->

<!-- BEGIN # MODAL LOGIN -->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" align="center">
                <img class="img-circle" id="img_logo" src="http://bootsnipp.com/img/logo.jpg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                   
                </button>
            </div>

            <!-- Begin # DIV Form -->
            <div id="div-forms">
                <c:url var="loginUrl" value="/login"></c:url>
                <form name='loginForm'
                      action="${loginUrl}" method='POST'>
                    <div class="modal-body">
                        <div id="div-login-msg">
                            <div id="icon-login-msg" class="glyphicon glyphicon-chevron-right"></div>
                            <span id="text-login-msg">Type your username and password.</span>
                        </div>
                        <input id="login_username" class="form-control" type="text" name="username" placeholder="Username" required>
                        <input id="login_password" class="form-control" type="password" name="password" placeholder="Password" required>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Remember me
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div>
                            
                           
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Login</button>
                        </div>
                                            </div>
                    <input type="hidden"
                           name="${_csrf.parameterName}" value="${_csrf.token}" />
                </form>



        </div>
    </div>
</div>
</div>