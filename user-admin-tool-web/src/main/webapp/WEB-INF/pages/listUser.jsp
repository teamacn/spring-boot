<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: root
  Date: 31/05/2016
  Time: 09:22
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <title>Show All Users</title>
</head>
<body ng-app="myApp">
<%@ include file ="/WEB-INF/include/lib.jsp" %>
<div ng-controller="UserController as userCtrl">
    <div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading"><span class="lead">List of Users </span></div>
    <div class="tablecontainer">
    <table class="table table-hover">
    <thead>
    <tr>
    <th>ID.</th>
    <th>Username</th>
    <th>First name</th>
    <th>Last name</th>
    <th>Email Address</th>
    <th>Role</th>
    <th width="20%"></th>
    </tr>
    </thead>
    <tbody>
    <tr ng-repeat="u in userCtrl.users">
    <td><span ng-bind="u.id"></span></td>
    <td><span ng-bind="u.username"></span></td>
    <td><span ng-bind="u.firstname"></span></td>
    <td><span ng-bind="u.lastname"></span></td>
        <td><span ng-bind="u.email"></span></td>
        <td><span ng-repeat="role in u.roles">
            <span ng-bind="role.role"></span>
        </span></td>
    <button type="button" ng-click="userCtrl.edit(u.id)" class="btn btn-success custom-width">Edit</button>  <button type="button" ng-click="ctrl.remove(u.id)" class="btn btn-danger custom-width">Remove</button>
    </td>
    </tr>
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
    <script src="<c:url value='/static/app/app.js' />"></script>
    <script src="<c:url value='/static/app/user_service.js' />"></script>
    <script src="<c:url value='/static/app/user_controller.js' />"></script>
</div>

<p>
    <a href="/user">Add User</a>
</p>
</body>
</html>
