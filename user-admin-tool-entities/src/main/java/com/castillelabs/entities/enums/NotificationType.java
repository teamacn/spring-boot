/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castillelabs.entities.enums;

/**
 *
 * @author root
 */
public enum NotificationType {
    REGISTRATION_NOTIFICATION,
    NEW_USER_NOTIFICATION,
    UPDATED_DETAILS_NOTIFICATION; 

    public static NotificationType getREGISTRATION_NOTIFICATION() {
        return REGISTRATION_NOTIFICATION;
    }

    public static NotificationType getNEW_USER_NOTIFICATION() {
        return NEW_USER_NOTIFICATION;
    }

    public static NotificationType getUPDATED_DETAILS_NOTIFICATION() {
        return UPDATED_DETAILS_NOTIFICATION;
    }
    
    
    
    
}
