package com.castillelabs.entities.user;

import com.castillelabs.entities.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.*;

/**
 * @author noorefatemah.suhabuth@castillelabs.com on 31/05/2016.
 */

@Entity
@Table(name = "user_role")
@XmlRootElement
@XmlAccessorType(value = XmlAccessType.FIELD)
public class Role extends AbstractEntity implements Serializable{

    @Column(name = "role")
    @XmlAttribute(required = true)
    private String roleName;

    @ManyToOne(cascade=CascadeType.ALL)
    @JsonBackReference
    @XmlTransient
    private User user;
    public Role() {/*default constructor*/}


    public void setUser(User user) {
        this.user = user;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

   
}
