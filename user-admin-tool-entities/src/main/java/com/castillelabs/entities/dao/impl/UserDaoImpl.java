//package com.castillelabs.entities.dao.impl;
//
//import com.castillelabs.entities.dao.UserDao;
//import com.castillelabs.entities.user.User;
//import java.util.List;
//import javax.persistence.EntityManager;
//import org.hibernate.SessionFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//
///**
// * @author noorefatemah.suhabuth@castillelabs.com on 01/06/2016.
// */
//@Repository
//
//public class UserDaoImpl extends AbstractDaoImpl<User> implements UserDao {
//
//    @Autowired
//    private EntityManager entityManager;
//
//    @Override
//    public EntityManager getEntityManager() {
//        return entityManager;
//    }
//
//    @Override
//    public void setEntityManager(EntityManager entityManager) {
//        this.entityManager = entityManager;
//    }
//
//    @Override
//    public User searchUserByUsername(String username) {
//        return (User) entityManager.createQuery(
//                "from User where username = :username")
//                .setParameter("username", username)
//                .getSingleResult();
//    }
//
//    @Override
//    public boolean isUserExist(String username) {
//        List<User> user = entityManager.createQuery(
//                "from User where username = :username")
//                .setParameter("username", username)
//                .getResultList();
//
//        if (user.isEmpty()) {
//            return false;
//        } else {
//            return true;
//        }
//
//    }
//
//    @Override
//    public List<User> searchByRole(String roleName) {
//        List<User> user = (List<User>) entityManager.createQuery(
//                "SELECT u FROM User u, Role roles where u.id  = roles.user and LOWER(roles.roleName) like :role ")
//                .setParameter("role", "%" + roleName.toLowerCase() + "%")
//                .getResultList();
//        return user;
//
//    }
//
//    @Override
//    public List<User> searchByLastName(String lastName) {
//        List<User> user = (List<User>) entityManager.createQuery("select u from User u "
//                + "where LOWER(u.lastName) like :lastname")
//                .setParameter("lastname", "%" + lastName.toLowerCase() + "%")
//                .getResultList();
//
//        return user;
//    }
//
//    @Override
//    public List<User> searchByFirstName(String firstName) {
//        List<User> user = (List<User>) entityManager.createQuery("select u from User u "
//                + "where LOWER(u.firstName) like :firstname")
//                .setParameter("firstname", "%" + firstName.toLowerCase() + "%")
//                .getResultList();
//
//        return user;
//    }
//
//    @Override
//    public List<User> searchUserListByUsername(String username) {
//
//        List<User> user = (List<User>) entityManager.createQuery("select u from User u "
//                + "where LOWER(u.username) like :username")
//                .setParameter("username", "%" + username.toLowerCase() + "%")
//                .getResultList();
//        return user;
//    }
//
//    @Override
//    public List<User> retrieveAll() {
//         return entityManager.createQuery("from User").getResultList();
//    }
//
//}
