package com.castillelabs.entities.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.castillelabs.entities.user.User;

public interface UserRepository extends CrudRepository<User, Long>{
		
	@Query("select u from User u where u.username=?1")
	  public User searchUserByUsername(String username); 

	@Query("select u from User u where u.username=?1")
	    public List<User> isUserExist(String username) ;
	        
	@Query("SELECT u from User u , Role r where u.id=r.user and LOWER(r.roleName) LIKE CONCAT('%',:roleName,'%')")
	    public List<User> searchByRole(@Param("roleName") String roleName);

	@Query("SELECT u from User u where LOWER(u.firstName) LIKE CONCAT('%',:firstName,'%')")
	public List<User> searchByFirstName(@Param("firstName") String firstName) ;
	      
	@Query("SELECT u from User u where LOWER(u.lastName) LIKE CONCAT('%',:lastName,'%')")
	public List<User> searchByLastName(@Param("lastName") String lastName) ;
	
	@Query("SELECT u from User u where LOWER(u.username) LIKE CONCAT('%',:username,'%')")
	public List<User> searchByUsername(@Param("username") String username) ;

	@Query("SELECT u FROM User u")
	    public List<User> retrieveAll() ;
	

}
