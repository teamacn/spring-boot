//package com.castillelabs.entities.dao.impl;
//
//import com.castillelabs.entities.dao.Dao;
//import java.lang.reflect.ParameterizedType;
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//import org.springframework.transaction.annotation.Transactional;
//
///**
// * @author noorefatemah.suhabuth@castillelabs.com on 07/06/2016.
// */
//
//public abstract class AbstractDaoImpl<T> implements Dao<T> {
//
//@PersistenceContext
//private EntityManager entityManager; 
// 
//    protected Class<T> entityType;
//
//    public AbstractDaoImpl() {
//        this.entityType = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
//    }
//
//    public EntityManager getEntityManager() {
//        return entityManager;
//    }
//
//    public void setEntityManager(EntityManager entityManager) {
//        this.entityManager = entityManager;
//    }
//
//    @Override
//    public void persist(T object) {
//       entityManager.persist(object);
//    }
//
//@Override
//    public T saveOrUpdate(T object) {
//      return entityManager.merge(object);
//    }
//    
//@Override
//    public T retrieveById(Long id) {
//      return entityManager.find(entityType, id);
//
//        
//    }
//
//
////  @SuppressWarnings("unchecked")
////  public List getAll() {
////    return entityManager.createQuery("from User").getResultList();
////  }
////  
//  
//@Override
//  public boolean delete(T object) {
//    if (entityManager.contains(object)){
//      entityManager.remove(object);
//    return true;}
//    else{
//      entityManager.remove(entityManager.merge(object));
//    return false;}
//  }
//
//   
//
//    
//}
