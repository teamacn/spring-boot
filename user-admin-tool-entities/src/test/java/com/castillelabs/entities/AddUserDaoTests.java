//package com.castillelabs.entities;
//
//
//import com.castillelabs.entities.dao.UserDao;
//import com.castillelabs.entities.user.Role;
//import com.castillelabs.entities.user.User;
//import com.google.common.collect.Iterables;
//import java.util.ArrayList;
//import java.util.List;
//import org.hibernate.exception.ConstraintViolationException;
//import org.junit.*;
//import org.junit.rules.ExpectedException;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//
///**
// * @author noorefatemah.suhabuth@castillelabs.com on 04/08/2016.
// */
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"classpath:/application-context-test.xml"})
//public class AddUserDaoTests extends AbstractTransactionalJUnit4SpringContextTests {
//    @Autowired
//    private UserDao userDao;
//    @Rule
//    public final ExpectedException exception = ExpectedException.none();
//
//    @Before
//    public void setUp(){
//        executeSqlScript("createtables.sql",true);
//        executeSqlScript("insert.sql", true);
//        executeSqlScript("insertRole.sql", true);
//
//
//    }
//
//    public User populateData(){
//        Role role = new Role();
//        Role role1 = new Role();
//        role1.setRoleName("ROLE_REST");
//        List<Role> roles = new ArrayList<Role>();
//        role.setRoleName("ROLE_EDITOR");
//        roles.add(role);
//        roles.add(role1);
//        User user = new User("noorefatemah", "Suhabuth", "noore_s", "password", "noorefatemah.suhabuth@gmail.com", roles, true);
//        role.setUser(user);
//        return user;
//    }
//
//    @After
//    public void tearDown(){
//
//        executeSqlScript("deleteRole.sql", true);
//        executeSqlScript("delete.sql", true);
//
//    }
//    @Test
//    public void checkAddedDetails() {
//        User user = populateData();
//        userDao.persist(user);
//        user.setSaved(true);
//        List<User> users = userDao.retrieveAll();
//        users.size();
//
//        Assert.assertEquals(user.getLastName() ,users.get(1).getLastName());
//        Assert.assertEquals(user.getFirstName() ,users.get(1).getFirstName());
//        Assert.assertEquals(user.getLastName() ,users.get(1).getLastName());
//        Assert.assertEquals(user.getPassword() ,users.get(1).getPassword());
//        Assert.assertEquals(user.getEmailAddress() ,users.get(1).getEmailAddress());
//        Assert.assertEquals(user.getUsername() ,users.get(1).getUsername());
//        Assert.assertEquals(user.getRoles().get(0).getRoleName() , Iterables.get(users.get(1).getRoles(),0).getRoleName());
//        Assert.assertEquals(user.getRoles().get(1).getRoleName() , Iterables.get(users.get(1).getRoles(),1).getRoleName());
//        Assert.assertEquals(true, user.isSaved());
//    }
//    @Test()
//
//    public void checkExceptionForDuplicate() {
//    User user = populateData();
//        user.setUsername("noore");
//        exception.expect(ConstraintViolationException.class);
//        userDao.persist(user);
//        user.setSaved(false);
////        Assert.assertEquals(false, user.isSaved());
//
//    }
//
//    @Test
//    public void testUserExist(){
//        Boolean userExist = userDao.isUserExist("noore");
//        Assert.assertEquals(true,userExist);
//    }
//
//    @Test
//    public void testUserDoesNotExists() {
//
//        Boolean userExist = userDao.isUserExist("noorefatemah_suhabuth");
//        Assert.assertEquals(false, userExist);
//    }
//}
//
//
//
